<?php

/**
 * @file
 * Administrative page callbacks for the Krux Tags module.
 */

/**
 * Form constructor for the Krux Tags Config form.
 */
function krux_tags_configuration_form($form, &$form_state) {
  $form = array();

  $form['krux_tags_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Krux Tags Site Name.'),
    '#description' => t('Enter your Krux Tags Site Name'),
    '#default_value' => variable_get('krux_tags_site_name', variable_get('site_name', '')),
  );

  $form['krux_tags_data_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Krux Tags Data ID.'),
    '#description' => t('Enter your Krux Tags Data ID.'),
    '#default_value' => variable_get('krux_tags_data_id', ''),
  );

  $form['krux_tags_script_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Krux Tags Script URI.'),
    '#description' => t('Enter your Krux Tags Script URI,<br>e.g: //cdn.krxd.net/controltag/asdfhjkl.js'),
    '#default_value' => variable_get('krux_tags_script_uri', ''),
  );

  return system_settings_form($form);
}
